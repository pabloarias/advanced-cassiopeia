<?php

/**
 * @package     Joomla.Site
 * @subpackage  Templates.cassiopeia
 *
 * @copyright   (C) 2017 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;

/** @var Joomla\CMS\Document\HtmlDocument $this */

$app   = Factory::getApplication();
$input = $app->getInput();
$wa    = $this->getWebAssetManager();

// Favicon
$faviconFile = $this->params->get('faviconFile');
if (!empty($faviconFile)) {
    $this->addHeadLink($faviconFile, 'icon', 'rel');
}

// Detecting Active Variables
$option   = $input->getCmd('option', '');
$view     = $input->getCmd('view', '');
$layout   = $input->getCmd('layout', '');
$task     = $input->getCmd('task', '');
$itemid   = $input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');
$menu     = $app->getMenu()->getActive();
$pageclass = $menu !== null ? $menu->getParams()->get('pageclass_sfx', '') : '';

// Get params from template options
$primaryColor = $this->params->get('primaryColor');
$ctaColor = $this->params->get('ctaColor');
$lightColor = $this->params->get('lightColor');
$darkColor = $this->params->get('darkColor');
$safeFontsVariable = '--safe-fonts: ' . $this->params->get('safeFonts') . '; ';
$paramsTemplateColorsArray = $this->params->get('templateColors', false);
$paramsTemplateFontsArray = $this->params->get('templateFonts', false);

// Transform Cassiopeia variables with params chosen in backend options
$cassiopeiaVariables =      '--cassiopeia-color-primary: ' . $primaryColor . '; ' . 
                            '--cassiopeia-color-brand: ' . $ctaColor . '; ' .
                            '--cassiopeia-color-link: ' . $ctaColor . '; ' . 
                            '--cassiopeia-color-hover: ' . $primaryColor . '; ' .
                            '--hue: 214;' .
                            '--template-bg-light: ' . $lightColor . '; ' .
                            '--template-text-dark: ' . $darkColor . '; ' .
                            '--template-text-light: ' . $lightColor . '; ' .
                            '--template-link-color: ' . $primaryColor . '; ' .
                            '--template-special-color: ' . $ctaColor . '; ' .
                            '--body-font-family: ' . $this->params->get('bodyTextFont') . '; ' .
                            '--body-font-size: ' . $this->params->get('bodyTextSize') . '; ' .
                            '--cassiopeia-font-family-body: ' . $this->params->get('bodyTextFont') . '; ' .
                            '--cassiopeia-font-family-headings: ' . $this->params->get('headersTextFont') . '; ' ;

// Generate color variables and classes
$tmplParamsColorVariables = '--primary-color: ' . $primaryColor . '; ' . 
                            '--cta-color: ' . $ctaColor . '; ' .
                            '--light-color: ' . $lightColor . '; ' .
                            '--dark-color: ' . $darkColor . '; ';
$tmplParamsColorsClasses =  '.primary-color {color: ' . $primaryColor . ';} ' .
                            '.cta-color {color: ' . $ctaColor . ';} ' .
                            '.light-color {color: ' . $lightColor . ';} ' .
                            '.dark-color {color: ' . $darkColor . ';} ';

$tmplFontDefinition = '';
$tmplColorClasses = '';
$tmplFontClasses = '';
$tmplFontVariables = '';
if (!empty($paramsTemplateColorsArray) || !empty($paramsTemplateFontsArray)) {
    if (!empty($paramsTemplateColorsArray)) {
        foreach ($paramsTemplateColorsArray as $paramsTemplateColor) {
            $tmplParamsColorVariables .= '--' . $paramsTemplateColor->colorName . ': ' . $paramsTemplateColor->colorValue . '; ';
            $tmplColorClasses .= '.' . $paramsTemplateColor->colorName . ' {color: ' . $paramsTemplateColor->colorValue . ';} ';
        }
    }
    if (!empty($paramsTemplateFontsArray)) {
        foreach ($paramsTemplateFontsArray as $paramsTemplateFont) {
            $tmplFontDefinition .=  '@font-face {' .
                                        'font-family: "' . $paramsTemplateFont->fontName . '"; ' .
                                        'font-style: ' . $paramsTemplateFont->fontStyle . '; ' .
                                        'font-weight: ' . $paramsTemplateFont->fontWeight . '; ' .
                                        'src: url("' . Uri::root(false) . 'media/templates/site/advanced_cassiopeia/fonts/' . $paramsTemplateFont->fontFile . '"); ' .
                                        'format: ("woff2"); font-display: swap;' .
                                    '} ';
            $tmplFontClasses .= ' .' . $paramsTemplateFont->fontName . ' {font-family: "' . $paramsTemplateFont->fontName . '";} ';
            $tmplFontVariables .= '--' . $paramsTemplateFont->fontName . ': ' . $paramsTemplateFont->fontName . '; ';
        }
    }
}

// Texts params
$textsFontAndColorCSS = 'body {font-family: ' . $this->params->get('bodyTextFont') . '; color: ' . $this->params->get('bodyTextColor') . ';} ' .
'h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6 {font-family: ' . $this->params->get('headersTextFont') . '; color: ' . $this->params->get('headersTextColor') . ';} ' .
'h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, .h1 a, .h2 a, .h3 a, .h4 a, .h5 a, .h6 a {color: ' . $this->params->get('headersLinkTextColor') . ';} ' .
'a {color: ' . $this->params->get('linksTextColor') . ';} ' .
'a:focus, a:hover, a:not(.btn):hover, a:not(.btn):focus {color: ' . $this->params->get('linksHoverTextColor') . ';} ' .
'p {hyphens: ' . $this->params->get('hyphensValue') . ';} ';

// Header CSS
if ($this->params->get('headerBgType') == 'plainColor') {
    $headerBgCSS = 'background-color: ' . $this->params->get('headerBgColor') . '; background-image: none; ';
} else {
    $headerBgCSS = 'background-color: ' . $this->params->get('headerBgColor') . '; background-image: ' . $this->params->get('headerBgGradient') . '; ';
}
$headerBoxShadow = 'box-shadow: ' . $this->params->get('headerBoxShadow') . ';';
$headerTextCSS = 'color: ' . $this->params->get('headerTextColor') . '; font-family: ' . $this->params->get('headerTextFont') . ';';
$headerLinksCSS = '.container-header a {color:' . $this->params->get('headerLinksColor') . ';} ';
$headerLinksHoverCSS = '.container-header a:hover, .container-header .active a {color:' . $this->params->get('headerLinksHoverColor') . ';} ';
$logoMaxWidth = '.navbar-brand {max-width:' . $this->params->get('logoMaxWidth') . ';}';
$headerCSS = '.container-header {' . $headerBgCSS . $headerTextCSS . $headerBoxShadow . '} ' . $headerLinksCSS . $headerLinksHoverCSS . $logoMaxWidth;

// Menu color and font params
$menuTextColor = $this->params->get('menuTextColor');
$menuActiveTextColor = $this->params->get('menuActiveTextColor');

$menuCSS = '.container-header .mod-menu > li > a, ' .
           '.container-header .mod-menu > li > span, ' .
           '.metismenu.mod-menu .mm-toggler {color:' . $menuTextColor . ';}' .
           '.container-header .mod-menu li.active > a, ' .
           '.container-header .mod-menu > li.active > span {color:' . $menuActiveTextColor . ';}' .
           '.container-header .metismenu > li.active > a::after, .container-header .metismenu > li > a:hover::after, ' .
           '.container-header .mod-menu > li.active::after, .container-header .mod-menu > li:hover::after {background-color: ' . $menuActiveTextColor . ';}' .
           '.container-header .navbar-toggler {border: 1px solid ' . $menuTextColor . '; color:' . $menuTextColor . ';}' .
           '.container-header .metismenu.mod-menu .mm-collapse {background-color:' . $this->params->get('submenuBgColor') . ';}' .
           '.container-header .metismenu.mod-menu .mm-collapse a {color:' . $this->params->get('submenuTextColor') . ';}';

// Footer CSS
if ($this->params->get('footerBgType') == 'plainColor') {
    $footerBgCSS = 'background-color: ' . $this->params->get('footerBgColor') . '; background-image: none; ';
} else {
    $footerBgCSS = 'background-color: ' . $this->params->get('footerBgColor') . '; background-image: ' . $this->params->get('footerBgGradient') . '; ';
}
$footerBoxShadow = 'box-shadow: ' . $this->params->get('footerBoxShadow') . ';';
$footerTextCSS = 'color: ' . $this->params->get('footerTextColor') . '; font-family: ' . $this->params->get('footerTextFont') . ';';
$footerLinksCSS = '.container-footer a {color:' . $this->params->get('footerLinksColor') . ';} ';
$footerLinksHoverCSS = '.container-footer a:hover, .container-footer .active a {color:' . $this->params->get('footerLinksHoverColor') . ';} ';
$footerCSS = '.container-footer {' . $footerBgCSS . $footerTextCSS . $footerBoxShadow . '} ' . $footerLinksCSS . $footerLinksHoverCSS;

// Width params
$headerFooterMaxWidth = '.grid-child {max-width: ' . $this->params->get('headerFooterMaxWidth') . ';}';
$siteGridColumnsWidth = '@supports (display:grid) {.site-grid {' .
    'grid-template-columns:[full-start] minmax(0,1fr) [main-start] repeat(4,minmax(0,' . $this->params->get('siteGridColumnsWidth') . ')) [main-end] minmax(0,1fr) [full-end];}}';

// Send params to assets
$tmplStyleVariables = ':root {' . $safeFontsVariable . $cassiopeiaVariables . $tmplParamsColorVariables . $tmplFontVariables . '} ';
$templateOptionsToStyles = $tmplFontDefinition . $tmplStyleVariables . $tmplParamsColorsClasses . $textsFontAndColorCSS . $menuCSS .
                           $tmplColorClasses . $tmplFontClasses . $headerCSS . $footerCSS . $headerFooterMaxWidth . $siteGridColumnsWidth;
if (!empty($this->params->get('customCodeCSS'))) { $templateOptionsToStyles .= $this->params->get('customCodeCSS'); };
$wa->addInlineStyle($templateOptionsToStyles, ['position' => 'before'], [], ['template.user']);
if (!empty($this->params->get('customCodeJS'))) { $wa->addInlineScript($this->params->get('customCodeJS')); };


// Enable assets
$wa->usePreset('template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr'))
    ->useStyle('template.active.language')
    ->useStyle('template.user')
    ->useScript('template.user');

// Override 'template.active' asset to set correct ltr/rtl dependency
$wa->registerStyle('template.active', '', [], [], ['template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr')]);

// Add new CSS file for Advanced Cassiopeia template
$wa->registerAndUseStyle('advanced_cassiopeia', 'media/templates/site/advanced_cassiopeia/css/advanced_cassiopeia.css', [], [], []);

// Logo file or site title param
if ($this->params->get('logoFile')) {
    $logo = HTMLHelper::_('image', Uri::root(false) . htmlspecialchars($this->params->get('logoFile'), ENT_QUOTES), $sitename, ['loading' => 'eager', 'decoding' => 'async'], false, 0);
} elseif ($this->params->get('siteTitle')) {
    $logo = '<span title="' . $sitename . '">' . htmlspecialchars($this->params->get('siteTitle'), ENT_COMPAT, 'UTF-8') . '</span>';
} else {
    $logo = HTMLHelper::_('image', 'logo.svg', $sitename, ['class' => 'logo d-inline-block', 'loading' => 'eager', 'decoding' => 'async'], true, 0);
}

$hasClass = '';

if ($this->countModules('sidebar-left', true)) {
    $hasClass .= ' has-sidebar-left';
}

if ($this->countModules('sidebar-right', true)) {
    $hasClass .= ' has-sidebar-right';
}

// Container
$wrapper = $this->params->get('fluidContainer') ? 'wrapper-fluid' : 'wrapper-static';

$this->setMetaData('viewport', 'width=device-width, initial-scale=1');

$stickyHeader = $this->params->get('stickyHeader') ? 'position-sticky sticky-top' : '';

// Defer fontawesome for increased performance. Once the page is loaded javascript changes it to a stylesheet.
$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <?php if (!empty($this->params->get('codeAfterHead'))) {echo $this->params->get('codeAfterHead');} ?>
    <jdoc:include type="metas" />
    <jdoc:include type="styles" />
    <jdoc:include type="scripts" />
    <?php if (!empty($this->params->get('codeBeforeFinishHead'))) {echo $this->params->get('codeBeforeFinishHead');} ?>
</head>

<body class="site <?php echo $option
    . ' ' . $wrapper
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($pageclass ? ' ' . $pageclass : '')
    . $hasClass
    . ($this->direction == 'rtl' ? ' rtl' : '');
?>">
    <?php if (!empty($this->params->get('codeAfterBody'))) {echo $this->params->get('codeAfterBody');} ?>
    <header class="header container-header full-width<?php echo $stickyHeader ? ' ' . $stickyHeader : ''; ?>">

        <?php if ($this->countModules('topbar')) : ?>
            <div class="container-topbar">
                <div class="grid-child">
                    <jdoc:include type="modules" name="topbar" style="none" />
                </div>
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('below-top')) : ?>
            <div class="container-below-top">
                <div class="grid-child">
                    <jdoc:include type="modules" name="below-top" style="none" />
                </div>
            </div>
        <?php endif; ?>

        <?php if ($this->params->get('brand', 1)) : ?>
            <div class="grid-child">
                <div class="navbar-brand">
                    <a class="brand-logo" href="<?php echo $this->baseurl; ?>/">
                        <?php echo $logo; ?>
                    </a>
                    <?php if ($this->params->get('siteDescription')) : ?>
                        <div class="site-description"><?php echo htmlspecialchars($this->params->get('siteDescription')); ?></div>
                    <?php endif; ?>
                </div>
                <?php if ($this->countModules('menu-right', true)) : ?>
                    <div class="menu-right">
                        <jdoc:include type="modules" name="menu-right" style="none" />
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('menu', true) || $this->countModules('search', true)) : ?>
            <div class="grid-child container-nav">
                <?php if ($this->countModules('menu', true)) : ?>
                    <jdoc:include type="modules" name="menu" style="none" />
                <?php endif; ?>
                <?php if ($this->countModules('search', true)) : ?>
                    <div class="container-search">
                        <jdoc:include type="modules" name="search" style="none" />
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </header>

    <div class="site-grid">
        <?php if ($this->countModules('banner', true)) : ?>
            <div class="container-banner full-width">
                <jdoc:include type="modules" name="banner" style="none" />
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('top-a', true)) : ?>
        <div class="grid-child container-top-a">
            <jdoc:include type="modules" name="top-a" style="card" />
        </div>
        <?php endif; ?>

        <?php if ($this->countModules('top-b', true)) : ?>
        <div class="grid-child container-top-b">
            <jdoc:include type="modules" name="top-b" style="card" />
        </div>
        <?php endif; ?>

        <?php if ($this->countModules('sidebar-left', true)) : ?>
        <div class="grid-child container-sidebar-left">
            <jdoc:include type="modules" name="sidebar-left" style="card" />
        </div>
        <?php endif; ?>

        <div class="grid-child container-component">
            <jdoc:include type="modules" name="breadcrumbs" style="none" />
            <jdoc:include type="modules" name="main-top" style="card" />
            <jdoc:include type="message" />
            <main>
                <jdoc:include type="component" />
            </main>
            <jdoc:include type="modules" name="main-bottom" style="card" />
        </div>

        <?php if ($this->countModules('sidebar-right', true)) : ?>
        <div class="grid-child container-sidebar-right">
            <jdoc:include type="modules" name="sidebar-right" style="card" />
        </div>
        <?php endif; ?>

        <?php if ($this->countModules('bottom-a', true)) : ?>
        <div class="grid-child container-bottom-a">
            <jdoc:include type="modules" name="bottom-a" style="card" />
        </div>
        <?php endif; ?>

        <?php if ($this->countModules('bottom-b', true)) : ?>
        <div class="grid-child container-bottom-b">
            <jdoc:include type="modules" name="bottom-b" style="card" />
        </div>
        <?php endif; ?>
    </div>

    <?php if ($this->countModules('footer', true)) : ?>
    <footer class="container-footer footer full-width">
        <div class="grid-child">
            <jdoc:include type="modules" name="footer" style="none" />
        </div>
    </footer>
    <?php endif; ?>

    <?php if ($this->params->get('backTop') == 1) : ?>
        <a href="#top" id="back-top" class="back-to-top-link" aria-label="<?php echo Text::_('TPL_CASSIOPEIA_BACKTOTOP'); ?>">
            <span class="icon-arrow-up icon-fw" aria-hidden="true"></span>
        </a>
    <?php endif; ?>

    <jdoc:include type="modules" name="debug" style="none" />
    <?php if (!empty($this->params->get('codeBeforeFinishBody'))) {echo $this->params->get('codeBeforeFinishBody');} ?>
</body>
</html>
