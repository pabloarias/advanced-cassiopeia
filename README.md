# Advanced Cassiopeia

Advanced Cassiopeia child template for Cassiopeia (the default Joomla template).

## Features

Lightweight and accessible Joomla template.

Easily change primary and CTA colors.

Choose as many colors as you want in template options. These colors will be available in frontend with CSS classes and variables (custom properties).

Choose header and footer width. Set site columns width too.

System safe fonts stack by default. Performance, no external servers, no privacy issues, no layout shifts. You can upload new fonts to a folder and select them to use in your site.

Added position menu-right to put main menu navigation right to the logo.

Specify logo max width.

Options to choose body, headers, menu and submenu fonts and colors.

Hyphens option to specify how words should be hyphenated when text wraps across multiple lines.

Select (or upload) a favicon from images folder.

Add custom CSS or Javascript code.

Customize header and footer background colors (or gradients), box-shadow, texts, links...

Headers and headers with links included.

Classes to align article images left or right:

- align-image-right: floats image right, limit width to 50 % and adds margin left and bottom.
- align-image-left: floats image left, limit width to 50 % and adds margin right and bottom.

These clases can be used for articles global option or individually.

## Installation

### Quick start

Create a child template from Cassiopeia (called "advanced") and copy these files on it.

### Long explanation

To create a child template go to System > Site Templates > Cassiopeia Details and Files > Create Child Template.

Name it "advanced". You can duplicate existing template styles if you want.

Now you have a new template folder named cassiopeia_advanced.

In media folder you have a new folder inside templates/site too.

Now, you can copy files from this template to your recently created child template.

Open template style and save it at least once.

## Upgrading

Copy files over existing ones from templates, media and languages.

Go to template styles, edit what you want and save.

## Available languages

English and Spanish. Want to contribute?

## Contributing

Tasks you can do to contribute:

- Use and test it. Open issues if you find errors.
- Translate template options to other languages. Basic settings are translated by Joomla core.

## License

GNU/GPL
