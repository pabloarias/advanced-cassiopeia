<?php

/**
 * @package     Joomla.Site
 * @subpackage  Templates.cassiopeia
 *
 * @copyright   (C) 2017 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\AuthenticationHelper;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;

/** @var Joomla\CMS\Document\HtmlDocument $this */

$extraButtons     = AuthenticationHelper::getLoginButtons('form-login');
$app              = Factory::getApplication();
$wa               = $this->getWebAssetManager();

$fullWidth = 1;

// Get params from template options
$primaryColor = $this->params->get('primaryColor');
$ctaColor = $this->params->get('ctaColor');
$lightColor = $this->params->get('lightColor');
$darkColor = $this->params->get('darkColor');
$safeFontsVariable = '--safe-fonts: ' . $this->params->get('safeFonts') . '; ';
$paramsTemplateColorsArray = $this->params->get('templateColors', false);
$paramsTemplateFontsArray = $this->params->get('templateFonts', false);

// Transform Cassiopeia variables with params chosen in backend options
$cassiopeiaVariables =      '--cassiopeia-color-primary: ' . $primaryColor . '; ' . 
                            '--cassiopeia-color-brand: ' . $ctaColor . '; ' .
                            '--cassiopeia-color-link: ' . $ctaColor . '; ' . 
                            '--cassiopeia-color-hover: ' . $primaryColor . '; ' .
                            '--hue: 214;' .
                            '--template-bg-light: ' . $lightColor . '; ' .
                            '--template-text-dark: ' . $darkColor . '; ' .
                            '--template-text-light: ' . $lightColor . '; ' .
                            '--template-link-color: ' . $primaryColor . '; ' .
                            '--template-special-color: ' . $ctaColor . '; ' .
                            '--body-font-family: ' . $this->params->get('bodyTextFont') . '; ' .
                            '--body-font-size: ' . $this->params->get('bodyTextSize') . '; ' .
                            '--cassiopeia-font-family-body: ' . $this->params->get('bodyTextFont') . '; ' .
                            '--cassiopeia-font-family-headings: ' . $this->params->get('headersTextFont') . '; ' ;

// Generate color variables and classes
$tmplParamsColorVariables = '--primary-color: ' . $primaryColor . '; ' . 
                            '--cta-color: ' . $ctaColor . '; ' .
                            '--light-color: ' . $lightColor . '; ' .
                            '--dark-color: ' . $darkColor . '; ';
$tmplParamsColorsClasses =  '.primary-color {color: ' . $primaryColor . ';} ' .
                            '.cta-color {color: ' . $ctaColor . ';} ' .
                            '.light-color {color: ' . $lightColor . ';} ' .
                            '.dark-color {color: ' . $darkColor . ';} ';

$tmplFontDefinition = '';
$tmplColorClasses = '';
$tmplFontClasses = '';
$tmplFontVariables = '';
if (!empty($paramsTemplateColorsArray) || !empty($paramsTemplateFontsArray)) {
    if (!empty($paramsTemplateColorsArray)) {
        foreach ($paramsTemplateColorsArray as $paramsTemplateColor) {
            $tmplParamsColorVariables .= '--' . $paramsTemplateColor->colorName . ': ' . $paramsTemplateColor->colorValue . '; ';
            $tmplColorClasses .= '.' . $paramsTemplateColor->colorName . ' {color: ' . $paramsTemplateColor->colorValue . ';} ';
        }
    }
    if (!empty($paramsTemplateFontsArray)) {
        foreach ($paramsTemplateFontsArray as $paramsTemplateFont) {
            $tmplFontDefinition .=  '@font-face {' .
                                        'font-family: "' . $paramsTemplateFont->fontName . '"; ' .
                                        'font-style: ' . $paramsTemplateFont->fontStyle . '; ' .
                                        'font-weight: ' . $paramsTemplateFont->fontWeight . '; ' .
                                        'src: url("' . Uri::root(false) . 'media/templates/site/cassiopeia_advanced/fonts/' . $paramsTemplateFont->fontFile . '"); ' .
                                        'format: ("woff2"); font-display: swap;' .
                                    '} ';
            $tmplFontClasses .= ' .' . $paramsTemplateFont->fontName . ' {font-family: "' . $paramsTemplateFont->fontName . '";} ';
            $tmplFontVariables .= '--' . $paramsTemplateFont->fontName . ': ' . $paramsTemplateFont->fontName . '; ';
        }
    }
}

// Texts params
$textsFontAndColorCSS = 'body {font-family: ' . $this->params->get('bodyTextFont') . '; color: ' . $this->params->get('bodyTextColor') . ';} ' .
'h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6 {font-family: ' . $this->params->get('headersTextFont') . '; color: ' . $this->params->get('headersTextColor') . ';} ' .
'h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, .h1 a, .h2 a, .h3 a, .h4 a, .h5 a, .h6 a {color: ' . $this->params->get('headersLinkTextColor') . ';} ' .
'a {color: ' . $this->params->get('linksTextColor') . ';} ' .
'a:focus, a:hover, a:not(.btn):hover, a:not(.btn):focus {color: ' . $this->params->get('linksHoverTextColor') . ';} ' .
'p {hyphens: ' . $this->params->get('hyphensValue') . ';} ';

// Header CSS
if ($this->params->get('headerBgType') == 'plainColor') {
    $headerBgCSS = 'background-color: ' . $this->params->get('headerBgColor') . '; background-image: none; ';
} else {
    $headerBgCSS = 'background-color: ' . $this->params->get('headerBgColor') . '; background-image: ' . $this->params->get('headerBgGradient') . '; ';
}
$headerBoxShadow = 'box-shadow: ' . $this->params->get('headerBoxShadow') . ';';
$headerTextCSS = 'color: ' . $this->params->get('headerTextColor') . '; font-family: ' . $this->params->get('headerTextFont') . ';';
$headerLinksCSS = '.container-header a {color:' . $this->params->get('headerLinksColor') . ';} ';
$headerLinksHoverCSS = '.container-header a:hover, .container-header .active a {color:' . $this->params->get('headerLinksHoverColor') . ';} ';
$logoMaxWidth = '.navbar-brand {max-width:' . $this->params->get('logoMaxWidth') . ';}';
$headerCSS = '.container-header {' . $headerBgCSS . $headerTextCSS . $headerBoxShadow . '} ' . $headerLinksCSS . $headerLinksHoverCSS . $logoMaxWidth;

// Menu color and font params
$menuTextColor = $this->params->get('menuTextColor');
$menuActiveTextColor = $this->params->get('menuActiveTextColor');

$menuCSS = '.container-header .mod-menu > li > a, ' .
           '.container-header .mod-menu > li > span, ' .
           '.metismenu.mod-menu .mm-toggler {color:' . $menuTextColor . ';}' .
           '.container-header .mod-menu > li.active > a, ' .
           '.container-header .mod-menu > li.active > span {color:' . $menuActiveTextColor . ';}' .
           '.container-header .metismenu > li.active > a::after, .container-header .metismenu > li > a:hover::after, ' .
           '.container-header .mod-menu > li.active::after, .container-header .mod-menu > li:hover::after {background-color: ' . $menuActiveTextColor . ';}' .
           '.container-header .navbar-toggler {border: 1px solid ' . $menuTextColor . '; color:' . $menuTextColor . ';}' .
           '.container-header .metismenu.mod-menu .mm-collapse {background-color:' . $this->params->get('submenuBgColor') . ';}' .
           '.container-header .metismenu.mod-menu .mm-collapse a {color:' . $this->params->get('submenuTextColor') . ';}';

// Footer CSS
if ($this->params->get('footerBgType') == 'plainColor') {
    $footerBgCSS = 'background-color: ' . $this->params->get('footerBgColor') . '; background-image: none; ';
} else {
    $footerBgCSS = 'background-color: ' . $this->params->get('footerBgColor') . '; background-image: ' . $this->params->get('footerBgGradient') . '; ';
}
$footerBoxShadow = 'box-shadow: ' . $this->params->get('footerBoxShadow') . ';';
$footerTextCSS = 'color: ' . $this->params->get('footerTextColor') . '; font-family: ' . $this->params->get('footerTextFont') . ';';
$footerLinksCSS = '.container-footer a {color:' . $this->params->get('footerLinksColor') . ';} ';
$footerLinksHoverCSS = '.container-footer a:hover, .container-footer .active a {color:' . $this->params->get('footerLinksHoverColor') . ';} ';
$footerCSS = '.container-footer {' . $footerBgCSS . $footerTextCSS . $footerBoxShadow . '} ' . $footerLinksCSS . $footerLinksHoverCSS;

// Width params
$headerFooterMaxWidth = '.grid-child {max-width: ' . $this->params->get('headerFooterMaxWidth') . ';}';
$siteGridColumnsWidth = '@supports (display:grid) {.site-grid {' .
    'grid-template-columns:[full-start] minmax(0,1fr) [main-start] repeat(4,minmax(0,' . $this->params->get('siteGridColumnsWidth') . ')) [main-end] minmax(0,1fr) [full-end];}}';

// Send params to assets
$tmplStyleVariables = ':root {' . $safeFontsVariable . $cassiopeiaVariables . $tmplParamsColorVariables . $tmplFontVariables . '} ';
$templateOptionsToStyles = $tmplFontDefinition . $tmplStyleVariables . $tmplParamsColorsClasses . $textsFontAndColorCSS . $menuCSS .
                           $tmplColorClasses . $tmplFontClasses . $headerCSS . $footerCSS . $headerFooterMaxWidth . $siteGridColumnsWidth;
if (!empty($this->params->get('customCodeCSS'))) { $templateOptionsToStyles .= $this->params->get('customCodeCSS'); };
$wa->addInlineStyle($templateOptionsToStyles, ['position' => 'before'], [], ['template.user']);
if (!empty($this->params->get('customCodeJS'))) { $wa->addInlineScript($this->params->get('customCodeJS')); };

// Enable assets
$wa->usePreset('template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr'))
    ->useStyle('template.active.language')
    ->useStyle('template.user')
    ->useScript('template.user');

// Override 'template.active' asset to set correct ltr/rtl dependency
$wa->registerStyle('template.active', '', [], [], ['template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr')]);

// Add new CSS file for Advanced Cassiopeia template
$wa->registerAndUseStyle('advanced_cassiopeia', 'media/templates/site/cassiopeia_advanced/css/advanced_cassiopeia.css', [], [], []);

// Logo file or site title param
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');

// Browsers support SVG favicons
$this->addHeadLink(HTMLHelper::_('image', 'joomla-favicon.svg', '', [], true, 1), 'icon', 'rel', ['type' => 'image/svg+xml']);
$this->addHeadLink(HTMLHelper::_('image', 'favicon.ico', '', [], true, 1), 'alternate icon', 'rel', ['type' => 'image/vnd.microsoft.icon']);
$this->addHeadLink(HTMLHelper::_('image', 'joomla-favicon-pinned.svg', '', [], true, 1), 'mask-icon', 'rel', ['color' => '#000']);

if ($this->params->get('logoFile')) {
    $logo = HTMLHelper::_('image', Uri::root(false) . htmlspecialchars($this->params->get('logoFile'), ENT_QUOTES), $sitename, ['loading' => 'eager', 'decoding' => 'async'], false, 0);
} elseif ($this->params->get('siteTitle')) {
    $logo = '<span title="' . $sitename . '">' . htmlspecialchars($this->params->get('siteTitle'), ENT_COMPAT, 'UTF-8') . '</span>';
} else {
    $logo = HTMLHelper::_('image', 'logo.svg', $sitename, ['class' => 'logo d-inline-block', 'loading' => 'eager', 'decoding' => 'async'], true, 0);
}

// Defer font awesome
$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <jdoc:include type="metas" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <jdoc:include type="styles" />
    <jdoc:include type="scripts" />
</head>
<body class="site">
    <div class="outer">
        <div class="offline-card">
            <div class="header">
            <?php if (!empty($logo)) : ?>
                <h1><?php echo $logo; ?></h1>
            <?php else : ?>
                <h1><?php echo $sitename; ?></h1>
            <?php endif; ?>
            <?php if ($app->get('offline_image')) : ?>
                <?php echo HTMLHelper::_('image', $app->get('offline_image'), $sitename, [], false, 0); ?>
            <?php endif; ?>
            <?php if ($app->get('display_offline_message', 1) == 1 && str_replace(' ', '', $app->get('offline_message')) != '') : ?>
                <p><?php echo $app->get('offline_message'); ?></p>
            <?php elseif ($app->get('display_offline_message', 1) == 2) : ?>
                <p><?php echo Text::_('JOFFLINE_MESSAGE'); ?></p>
            <?php endif; ?>
            </div>
            <div class="login">
                <jdoc:include type="message" />
                <form action="<?php echo Route::_('index.php', true); ?>" method="post" id="form-login">
                    <fieldset>
                        <label for="username"><?php echo Text::_('JGLOBAL_USERNAME'); ?></label>
                        <input name="username" class="form-control" id="username" type="text">

                        <label for="password"><?php echo Text::_('JGLOBAL_PASSWORD'); ?></label>
                        <input name="password" class="form-control" id="password" type="password">

                        <?php foreach ($extraButtons as $button) :
                            $dataAttributeKeys = array_filter(array_keys($button), function ($key) {
                                return substr($key, 0, 5) == 'data-';
                            });
                            ?>
                            <div class="mod-login__submit form-group">
                                <button type="button"
                                        class="btn btn-secondary w-100 mt-4 <?php echo $button['class'] ?? '' ?>"
                                <?php foreach ($dataAttributeKeys as $key) : ?>
                                    <?php echo $key ?>="<?php echo $button[$key] ?>"
                                <?php endforeach; ?>
                                <?php if ($button['onclick']) : ?>
                                    onclick="<?php echo $button['onclick'] ?>"
                                <?php endif; ?>
                                title="<?php echo Text::_($button['label']) ?>"
                                id="<?php echo $button['id'] ?>"
                                >
                                <?php if (!empty($button['icon'])) : ?>
                                    <span class="<?php echo $button['icon'] ?>"></span>
                                <?php elseif (!empty($button['image'])) : ?>
                                    <?php echo $button['image']; ?>
                                <?php elseif (!empty($button['svg'])) : ?>
                                    <?php echo $button['svg']; ?>
                                <?php endif; ?>
                                <?php echo Text::_($button['label']) ?>
                                </button>
                            </div>
                        <?php endforeach; ?>

                        <button type="submit" name="Submit" class="btn btn-primary"><?php echo Text::_('JLOGIN'); ?></button>

                        <input type="hidden" name="option" value="com_users">
                        <input type="hidden" name="task" value="user.login">
                        <input type="hidden" name="return" value="<?php echo base64_encode(Uri::base()); ?>">
                        <?php echo HTMLHelper::_('form.token'); ?>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
